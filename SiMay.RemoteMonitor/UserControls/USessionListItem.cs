﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SiMay.Net.SessionProvider.SessionBased;
using SiMay.RemoteMonitor.Entitys;

namespace SiMay.RemoteMonitor.UserControls
{
    public class USessionListItem : ListViewItem
    {
        public SessionSyncContext SyncContext { get; set; }
        public USessionListItem(SessionSyncContext syncContext)
        {
            SyncContext = syncContext;
            UpdateListItemText();
        }

        public void UpdateListItemText()
        {
            this.SubItems.Clear();
            this.Text = SyncContext.IPv4;
            this.SubItems.Add(SyncContext.MachineName);
            this.SubItems.Add(SyncContext.OSVersion);
            this.SubItems.Add(SyncContext.CpuInfo);
            this.SubItems.Add("1*" + SyncContext.CoreCount);
            this.SubItems.Add(SyncContext.MemroySize / 1024 / 1024 + "MB");
            this.SubItems.Add(SyncContext.AdminName);

            if (SyncContext.HasLoadServiceCOM)
            {
                this.SubItems.Add(SyncContext.IsCameraExist ? "YES" : "NO");
                this.SubItems.Add(SyncContext.IsRecordExist ? "YES" : "NO");
                this.SubItems.Add(SyncContext.IsPlayerExist ? "YES" : "NO");
            }
            else
            {
                //插件未加载
                this.SubItems.Add("Unknown");
                this.SubItems.Add("Unknown");
                this.SubItems.Add("Unknown");
            }
            this.SubItems.Add(SyncContext.Remark);
            this.SubItems.Add(SyncContext.Version);
            this.SubItems.Add(SyncContext.StarupDateTime);
            this.SubItems.Add(SyncContext.GroupName);
        }
    }
}
