﻿using SiMay.Core;
using SiMay.Core.Enums;
using SiMay.Core.PacketModelBinder.Attributes;
using SiMay.Core.PacketModelBinding;
using SiMay.Core.Packets;
using SiMay.Core.Packets.TcpConnection;
using SiMay.ServiceCore;
using SiMay.ServiceCore.Attributes;
using SiMay.ServiceCore.Extensions;
using SiMay.ServiceCore.ServiceSource;
using SiMay.Sockets.Tcp;
using SiMay.Sockets.Tcp.Session;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SiMay.ServiceCore.ControlService
{
    /// <summary>
    /// Key必须与控制源Key一致
    /// </summary>
    [ServiceKey("TcpConnectionManagerJob")]
    public class TcpConnectionManager : ServiceManager, IServiceSource
    {
        private PacketModelBinder<TcpSocketSaeaSession> _handlerBinder = new PacketModelBinder<TcpSocketSaeaSession>();

        /// <summary>
        /// 通知远程一切就绪
        /// </summary>
        [PacketHandler(MessageHead.S_GLOBAL_OK)]
        public void InitializeComplete(TcpSocketSaeaSession session)
        {
            SendAsyncToServer(MessageHead.C_MAIN_OPEN_DLG,
                new OpenDialogPack()
                {
                    IdentifyId = AppConfiguartion.IdentifyId,
                    ServiceKey = this.GetType().GetServiceKey(),
                    OriginName = Environment.MachineName + "@" + (AppConfiguartion.RemarkInfomation ?? AppConfiguartion.DefaultRemarkInfo)
                });
        }

        [PacketHandler(MessageHead.S_GLOBAL_ONCLOSE)]
        public void CloseSession(TcpSocketSaeaSession session)
        {
            CloseSession();
        }

        public override void OnNotifyProc(TcpSocketCompletionNotify notify, TcpSocketSaeaSession session)
        {
            switch (notify)
            {
                case TcpSocketCompletionNotify.OnConnected:
                    break;
                case TcpSocketCompletionNotify.OnSend:
                    break;
                case TcpSocketCompletionNotify.OnDataReceiveing:
                    break;
                case TcpSocketCompletionNotify.OnDataReceived:
                    this._handlerBinder.InvokePacketHandler(session, session.CompletedBuffer.GetMessageHead(), this);
                    break;
                case TcpSocketCompletionNotify.OnClosed:
                    this._handlerBinder.Dispose();
                    break;
            }
        }
        [PacketHandler(MessageHead.S_TCP_GET_LIST)]
        public void GetTcpConnectionList(TcpSocketSaeaSession session)
        {
            var table = GetTable();

            var connections = new TcpConnectionItem[table.Length];

            for (int i = 0; i < table.Length; i++)
            {
                string processName;
                try
                {
                    var p = Process.GetProcessById((int)table[i].owningPid);
                    processName = p.ProcessName;
                }
                catch
                {
                    processName = $"PID: {table[i].owningPid}";
                }
                var s = table[i];
                connections[i] = new TcpConnectionItem
                {
                    ProcessName = processName,
                    LocalAddress = table[i].LocalAddress.ToString(),
                    LocalPort = table[i].LocalPort.ToString(),
                    RemoteAddress = table[i].RemoteAddress.ToString(),
                    RemotePort = table[i].RemotePort.ToString(),
                    State = (TcpConnectionState)table[i].state
                };
                var ss = connections[i];
            }

            SendAsyncToServer(MessageHead.C_TCP_LIST, new TcpConnectionPack()
            {
                TcpConnections = connections
            });
        }

        [PacketHandler(MessageHead.S_TCP_CLOSE_CHANNEL)]
        public void CloseTcpConnectionHandler(TcpSocketSaeaSession session)
        {
            var kills = session.CompletedBuffer.GetMessageEntity<KillTcpConnectionPack>();

            var table = GetTable();

            foreach (var packet in kills.Kills)
            {
                var s = table.Where(c => c.RemotePort == 5200).ToList();
                for (var i = 0; i < table.Length; i++)
                {
                    //search for connection
                    if (packet.LocalAddress == table[i].LocalAddress.ToString() &&
                        packet.LocalPort == table[i].LocalPort.ToString() &&
                        packet.RemoteAddress == table[i].RemoteAddress.ToString() &&
                        packet.RemotePort == table[i].RemotePort.ToString())
                    {
                        // it will close the connection only if client run as admin
                        //table[i].state = (byte)ConnectionStates.Delete_TCB;
                        table[i].state = 12; // 12 for Delete_TCB state
                        var ptr = Marshal.AllocCoTaskMem(Marshal.SizeOf(table[i]));
                        Marshal.StructureToPtr(table[i], ptr, false);
                        var result = Win32Api.SetTcpEntry(ptr);
                    }
                }
            }
            this.GetTcpConnectionList(session);
        }
        private static Win32Api.MibTcprowOwnerPid[] GetTable()
        {
            Win32Api.MibTcprowOwnerPid[] tTable;
            var afInet = 2;
            var buffSize = 0;
            var ret = Win32Api.GetExtendedTcpTable(IntPtr.Zero, ref buffSize, true, afInet, Win32Api.TcpTableClass.TcpTableOwnerPidAll);
            var buffTable = Marshal.AllocHGlobal(buffSize);
            try
            {
                ret = Win32Api.GetExtendedTcpTable(buffTable, ref buffSize, true, afInet, Win32Api.TcpTableClass.TcpTableOwnerPidAll);
                if (ret != 0)
                    return null;
                var tab = (Win32Api.MibTcptableOwnerPid)Marshal.PtrToStructure(buffTable, typeof(Win32Api.MibTcptableOwnerPid));
                var rowPtr = (IntPtr)((long)buffTable + Marshal.SizeOf(tab.dwNumEntries));
                tTable = new Win32Api.MibTcprowOwnerPid[tab.dwNumEntries];
                for (var i = 0; i < tab.dwNumEntries; i++)
                {
                    var tcpRow = (Win32Api.MibTcprowOwnerPid)Marshal.PtrToStructure(rowPtr, typeof(Win32Api.MibTcprowOwnerPid));
                    tTable[i] = tcpRow;
                    rowPtr = (IntPtr)((long)rowPtr + Marshal.SizeOf(tcpRow));
                }
            }
            finally
            {
                Marshal.FreeHGlobal(buffTable);
            }
            return tTable;
        }
    }
}
